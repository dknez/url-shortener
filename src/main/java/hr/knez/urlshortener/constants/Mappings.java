package hr.knez.urlshortener.constants;

public class Mappings {

    public static final String ERROR_MAPPING = "/error";
    public static final String REDIRECT_MAPPING = "/r";
    public static final String SHORTENER_MAPPING = "/s";
}
