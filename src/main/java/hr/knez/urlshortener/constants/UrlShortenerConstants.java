package hr.knez.urlshortener.constants;

public class UrlShortenerConstants {

    public static final String URL_NOT_PROVIDED = "URL not provided!";
    public static final String URL_NOT_FOUND_IN_DB = "URL not found in the database.";

    public static final String STATUS_400_TEMPLATE = "400";
    public static final String STATUS_404_TEMPLATE = "404";
    public static final String DEFAULT_ERROR_TEMPLATE = "error";

    public static final String REDIRECT_PREFIX = "redirect:";
}
