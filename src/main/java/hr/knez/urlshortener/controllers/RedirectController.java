package hr.knez.urlshortener.controllers;

import static hr.knez.urlshortener.constants.Mappings.REDIRECT_MAPPING;
import static hr.knez.urlshortener.constants.UrlShortenerConstants.REDIRECT_PREFIX;

import hr.knez.urlshortener.exceptions.UrlShortenerException;
import hr.knez.urlshortener.services.UrlShortenerService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(REDIRECT_MAPPING)
public class RedirectController {

    private final UrlShortenerService urlShortenerService;

    public RedirectController(UrlShortenerService urlShortenerService) {
        this.urlShortenerService = urlShortenerService;
    }

    @RequestMapping("/{urlBase62}")
    public String redirectToOriginalUrl(@PathVariable String urlBase62)
        throws UrlShortenerException {
        return REDIRECT_PREFIX + getLongUrl(urlBase62);
    }

    private String getLongUrl(String urlIdBase62) throws UrlShortenerException {
        return urlShortenerService.getLongUrl(urlIdBase62);
    }

    @ExceptionHandler({UrlShortenerException.class, NumberFormatException.class})
    public void handleConstraintViolationException(HttpServletResponse response)
        throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }
}
