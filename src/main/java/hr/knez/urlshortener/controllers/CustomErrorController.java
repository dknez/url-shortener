package hr.knez.urlshortener.controllers;

import static hr.knez.urlshortener.constants.Mappings.ERROR_MAPPING;
import static hr.knez.urlshortener.constants.UrlShortenerConstants.DEFAULT_ERROR_TEMPLATE;
import static hr.knez.urlshortener.constants.UrlShortenerConstants.STATUS_400_TEMPLATE;
import static hr.knez.urlshortener.constants.UrlShortenerConstants.STATUS_404_TEMPLATE;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomErrorController implements ErrorController {

    @RequestMapping(ERROR_MAPPING)
    public String handleError(HttpServletRequest request) {
        Object errorStatus = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (errorStatus != null) {
            int statusCode = Integer.parseInt(errorStatus.toString());

            if (statusCode == HttpStatus.BAD_REQUEST.value()) {
                return STATUS_400_TEMPLATE;
            } else if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return STATUS_404_TEMPLATE;
            }
        }
        return DEFAULT_ERROR_TEMPLATE;
    }

    @Override
    public String getErrorPath() {
        return ERROR_MAPPING;
    }
}
