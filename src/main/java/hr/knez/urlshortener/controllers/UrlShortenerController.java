package hr.knez.urlshortener.controllers;

import static hr.knez.urlshortener.constants.Mappings.SHORTENER_MAPPING;

import hr.knez.urlshortener.services.UrlShortenerService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.xml.bind.ValidationException;
import org.hibernate.validator.constraints.URL;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = SHORTENER_MAPPING)
@Validated
public class UrlShortenerController {

    private final UrlShortenerService urlShortenerService;

    public UrlShortenerController(UrlShortenerService urlShortenerService) {
        this.urlShortenerService = urlShortenerService;
    }

    @GetMapping
    public HttpEntity<ResourceSupport> getShortUrl(
        @URL @NotBlank @RequestParam("url") String originalUrl) throws ValidationException {
        ResourceSupport shortUrl;
        shortUrl = urlShortenerService.getShortUrlResource(originalUrl);
        return ResponseEntity.ok(shortUrl);
    }

    @ExceptionHandler({ConstraintViolationException.class, ValidationException.class})
    public void handleConstraintViolationException(HttpServletResponse response)
        throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }
}
