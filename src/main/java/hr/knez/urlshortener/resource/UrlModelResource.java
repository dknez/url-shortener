package hr.knez.urlshortener.resource;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import org.springframework.hateoas.ResourceSupport;

/**
 * Represents the URL resource - REST representation of the URL model.
 *
 * @author domagoj
 */
public class UrlModelResource extends ResourceSupport {

    private final String shortUrl;
    private final String originalUrl;
    private final LocalDateTime created;
    private final Long hits;

    /**
     * Creates REST representation of the URLModel.
     */
    @JsonCreator
    public UrlModelResource(
        @JsonProperty("shortUrl") String shortUrl,
        @JsonProperty("longUrl") String originalUrl,
        @JsonProperty("created") LocalDateTime created,
        @JsonProperty("clicks") Long hits
    ) {
        this.shortUrl = shortUrl;
        this.originalUrl = originalUrl;
        this.created = created;
        this.hits = hits;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public Long getHits() {
        return hits;
    }
}
