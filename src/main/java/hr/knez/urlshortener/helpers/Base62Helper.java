package hr.knez.urlshortener.helpers;

import io.seruco.encoding.base62.Base62;
import org.springframework.stereotype.Component;

@Component
public class Base62Helper {

    private final Base62 base62 = Base62.createInstance();

    public Long decodeBase62(final String shortUrlId) {
        return Long.valueOf(new String(base62.decode(shortUrlId.getBytes())));
    }

    public String encodeBase62(final Long urlId) {
        return new String(base62.encode(String.valueOf(urlId).getBytes()));
    }
}
