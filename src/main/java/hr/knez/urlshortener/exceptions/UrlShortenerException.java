package hr.knez.urlshortener.exceptions;

public class UrlShortenerException extends Throwable {

    private final String errorMessage;

    public UrlShortenerException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public UrlShortenerException(String s, String errorMessage) {
        super(s);
        this.errorMessage = errorMessage;
    }
}
