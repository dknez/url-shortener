package hr.knez.urlshortener.model;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.springframework.lang.NonNull;

@Entity
public class UrlModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    private String originalUrl;

    private LocalDateTime created = LocalDateTime.now();

    @NonNull
    private Long hits = 0L;

    public UrlModel() {
    }

    public UrlModel(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public UrlModel(String originalUrl, LocalDateTime created, Long hits) {
        this.originalUrl = originalUrl;
        this.created = created;
        this.hits = hits;
    }

    public UrlModel incrementNumberOfHits() {
        hits++;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Long getHits() {
        return hits;
    }
}
