package hr.knez.urlshortener.repositories;

import hr.knez.urlshortener.model.UrlModel;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UrlRepository extends CrudRepository<UrlModel, Long> {

    Optional<UrlModel> findByOriginalUrl(String originalUrl);
}

