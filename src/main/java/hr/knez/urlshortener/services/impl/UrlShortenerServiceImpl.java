package hr.knez.urlshortener.services.impl;

import static hr.knez.urlshortener.constants.UrlShortenerConstants.URL_NOT_FOUND_IN_DB;
import static hr.knez.urlshortener.constants.UrlShortenerConstants.URL_NOT_PROVIDED;

import hr.knez.urlshortener.exceptions.UrlShortenerException;
import hr.knez.urlshortener.helpers.Base62Helper;
import hr.knez.urlshortener.model.UrlModel;
import hr.knez.urlshortener.repositories.UrlRepository;
import hr.knez.urlshortener.resource.UrlModelResource;
import hr.knez.urlshortener.services.UrlShortenerService;
import javax.xml.bind.ValidationException;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class UrlShortenerServiceImpl implements UrlShortenerService {

    private final Base62Helper base62Helper;
    private final UrlRepository urlRepository;

    public UrlShortenerServiceImpl(Base62Helper base62Helper, UrlRepository urlRepository) {
        this.base62Helper = base62Helper;
        this.urlRepository = urlRepository;
    }

    @Override
    public String getLongUrl(String urlIdBase62)
        throws UrlShortenerException, NumberFormatException {
        Long id = base62Helper.decodeBase62(urlIdBase62);

        UrlModel url = urlRepository.findById(id)
            .orElseThrow(() -> new UrlShortenerException(URL_NOT_FOUND_IN_DB))
            .incrementNumberOfHits();
        urlRepository.save(url);

        return url.getOriginalUrl();
    }

    @Override
    public ResourceSupport getShortUrlResource(String originalUrl) throws ValidationException {
        if (!isUrlProvided(originalUrl)) {
            throw new ValidationException(URL_NOT_PROVIDED);
        }
        UrlModel urlIndex = urlRepository.findByOriginalUrl(originalUrl)
            .orElseGet(() -> urlRepository.save(new UrlModel(originalUrl)));

        return new UrlModelResource(
            base62Helper.encodeBase62(urlIndex.getId()),
            urlIndex.getOriginalUrl(),
            urlIndex.getCreated(),
            urlIndex.getHits()
        );
    }

    private boolean isUrlProvided(String originalUrl) {
        return !StringUtils.isEmpty(originalUrl);
    }
}
