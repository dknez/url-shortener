package hr.knez.urlshortener.services;

import hr.knez.urlshortener.exceptions.UrlShortenerException;
import javax.xml.bind.ValidationException;
import org.springframework.hateoas.ResourceSupport;

public interface UrlShortenerService {

    String getLongUrl(String urlBase62) throws NumberFormatException, UrlShortenerException;

    ResourceSupport getShortUrlResource(String originalUrl) throws ValidationException;
}
