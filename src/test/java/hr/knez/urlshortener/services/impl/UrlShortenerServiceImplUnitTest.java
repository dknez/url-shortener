package hr.knez.urlshortener.services.impl;

import static hr.knez.urlshortener.constants.UrlShortenerConstants.URL_NOT_PROVIDED;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.DATE_CREATED;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.GOOGLE_URL;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSON_CLICK_COUNT_VALUE;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSON_GOOGLE_SHORT_URL_VALUE;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.SHORT_URL_ID;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.URL_MODEL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import hr.knez.urlshortener.exceptions.UrlShortenerException;
import hr.knez.urlshortener.helpers.Base62Helper;
import hr.knez.urlshortener.repositories.UrlRepository;
import hr.knez.urlshortener.resource.UrlModelResource;
import javax.xml.bind.ValidationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.hateoas.ResourceSupport;

@RunWith(MockitoJUnitRunner.class)
public class UrlShortenerServiceImplUnitTest {

    @InjectMocks
    private UrlShortenerServiceImpl urlShortenerService;

    @Mock
    private UrlRepository urlRepository;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private Base62Helper base62Helper;

    @Test
    public void getLongUrl_whenShortUrlIdDoesExistInDb_thenAppropriateResourceIsReturned()
        throws UrlShortenerException {
        given(base62Helper.decodeBase62(JSON_GOOGLE_SHORT_URL_VALUE)).willReturn(SHORT_URL_ID);
        given(urlRepository.findById(SHORT_URL_ID)).willReturn(java.util.Optional.of(URL_MODEL));

        String result = urlShortenerService.getLongUrl(JSON_GOOGLE_SHORT_URL_VALUE);

        assertThat(result).isEqualTo(GOOGLE_URL);
    }

    @Test
    public void getLongUrl_whenShortUrlIdDoesNotExistInDb_thenUrlShortenerExceptionIsThrown()
        throws UrlShortenerException {
        exception.expect(UrlShortenerException.class);
        given(base62Helper.decodeBase62(JSON_GOOGLE_SHORT_URL_VALUE)).willReturn(SHORT_URL_ID);

        urlShortenerService.getLongUrl(JSON_GOOGLE_SHORT_URL_VALUE);
    }

    @Test
    public void getShortUrl_whenUrlIsNotProvided_thenValidationExceptionIsThrown()
        throws ValidationException {
        exception.expect(ValidationException.class);
        exception.expectMessage(URL_NOT_PROVIDED);

        urlShortenerService.getShortUrlResource("");
    }

    @Test
    public void getShortUrl_whenRequestIsValid_thenValidResourceIsProvided()
        throws ValidationException {
        given(urlRepository.findByOriginalUrl(GOOGLE_URL))
            .willReturn(java.util.Optional.of(URL_MODEL));

        ResourceSupport result = urlShortenerService.getShortUrlResource(GOOGLE_URL);

        assertThat(((UrlModelResource) result).getOriginalUrl()).isEqualTo(GOOGLE_URL);
        assertThat(((UrlModelResource) result).getHits()).isEqualTo(JSON_CLICK_COUNT_VALUE + 1);
        assertThat(((UrlModelResource) result).getCreated()).isEqualTo(DATE_CREATED);
    }
}
