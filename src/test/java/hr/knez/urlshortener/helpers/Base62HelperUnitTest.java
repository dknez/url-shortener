package hr.knez.urlshortener.helpers;

import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.INVALID_SHORTURL;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSON_GOOGLE_SHORT_URL_VALUE;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.SHORT_URL_ID;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class Base62HelperUnitTest {

    @Spy
    Base62Helper base62Helper;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void decodeBase62_whenCorrectShortUrlValueIsGiven_thenCorrectShortUrlIdIsReturned() {
        int id = base62Helper.decodeBase62(JSON_GOOGLE_SHORT_URL_VALUE).intValue();

        assertEquals(id, SHORT_URL_ID);
    }

    @Test
    public void decodeBase62_whenIncorrectShortUrlValueIsGiven_thenNumberFormatExceptionIsReturned() {
        exception.expect(NumberFormatException.class);

        base62Helper.decodeBase62(INVALID_SHORTURL).intValue();
    }

    @Test
    public void decodeBase62_whenShortUrlValueIsEmptyString_thenNumberFormatExceptionIsReturned() {
        exception.expect(NumberFormatException.class);

        base62Helper.decodeBase62("").intValue();
    }

    @Test
    public void encodeBase62_whenCorrectShortUrlIdIsGiven_thenCorrectShortUrlValueIsReturned() {
        String shortUrl = base62Helper.encodeBase62(SHORT_URL_ID);

        assertNotNull(shortUrl);
        assertEquals(shortUrl, JSON_GOOGLE_SHORT_URL_VALUE);
    }
}
