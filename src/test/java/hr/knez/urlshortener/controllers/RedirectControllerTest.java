package hr.knez.urlshortener.controllers;

import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.GOOGLE_URL;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.INVALID_SHORTURL;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSON_GOOGLE_SHORT_URL_VALUE;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.REDIRECT_MAPPING;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import hr.knez.urlshortener.exceptions.UrlShortenerException;
import hr.knez.urlshortener.services.impl.UrlShortenerServiceImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(RedirectController.class)
public class RedirectControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UrlShortenerServiceImpl urlShortenerService;

    @Test
    public void redirectToOriginalUrl_whenCorrectShortUrlIsProvided_thenUserIsRedirected() throws Exception, UrlShortenerException {
        given(urlShortenerService.getLongUrl(JSON_GOOGLE_SHORT_URL_VALUE)).willReturn(GOOGLE_URL);

        mockMvc.perform(get(REDIRECT_MAPPING + JSON_GOOGLE_SHORT_URL_VALUE))
            .andDo(print())
            .andExpect(status().is3xxRedirection());
    }

    @Test
    public void redirectToOriginalUrl_whenShortUrlDoesNotExistInDb_thenStatusCode404IsReturned() throws Exception, UrlShortenerException {
        given(urlShortenerService.getLongUrl(INVALID_SHORTURL)).willThrow(UrlShortenerException.class);

        mockMvc.perform(get(REDIRECT_MAPPING + INVALID_SHORTURL))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void redirectToOriginalUrl_whenShortUrlIsNotProvided_thenStatusCode404IsReturned() throws Exception {
        mockMvc.perform(get(REDIRECT_MAPPING))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
}
