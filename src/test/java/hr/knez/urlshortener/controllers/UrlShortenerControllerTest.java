package hr.knez.urlshortener.controllers;

import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.DATE_CREATED;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.GOOGLE_SHORT_URL_RESOURCE;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.GOOGLE_URL;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.INVALID_URL;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSONPATH_CLICKS_KEY;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSONPATH_CREATED_KEY;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSONPATH_LONG_URL_KEY;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSONPATH_SHORT_URL_KEY;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSON_CLICK_COUNT_VALUE;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.JSON_GOOGLE_SHORT_URL_VALUE;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.SHORTEN_MAPPING;
import static hr.knez.urlshortener.constants.UrlShortenerTestConstants.URL_PARAMETER;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import hr.knez.urlshortener.repositories.UrlRepository;
import hr.knez.urlshortener.services.impl.UrlShortenerServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(UrlShortenerController.class)
public class UrlShortenerControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UrlShortenerServiceImpl urlShortenerService;
    @MockBean
    private UrlRepository urlRepository;

    @Test
    public void getShortUrl_whenValidUrlIsGiven_thenCorrectJsonIsReturned() throws Exception {
        given(urlShortenerService.getShortUrlResource(GOOGLE_URL))
            .willReturn(GOOGLE_SHORT_URL_RESOURCE);

        mockMvc.perform(get(SHORTEN_MAPPING + URL_PARAMETER + GOOGLE_URL))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
            .andExpect(jsonPath(JSONPATH_SHORT_URL_KEY).value(JSON_GOOGLE_SHORT_URL_VALUE))
            .andExpect(jsonPath(JSONPATH_LONG_URL_KEY).value(GOOGLE_URL))
            .andExpect(jsonPath(JSONPATH_CREATED_KEY).value(DATE_CREATED.toString()))
            .andExpect(jsonPath(JSONPATH_CLICKS_KEY).value(JSON_CLICK_COUNT_VALUE));
    }

    @Test
    public void getShortUrl_whenInvalidUrlIsGiven_thenStatusCode400IsReturned()
        throws Exception {
        mockMvc.perform(get(SHORTEN_MAPPING + URL_PARAMETER + INVALID_URL))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void getShortUrl_whenUrlParameterIsNotProvided_thenStatusCode400IsReturned()
        throws Exception {
        mockMvc.perform(get(SHORTEN_MAPPING))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void getShortUrl_whenUrlParameterIsEmpty_thenStatusCode400IsReturned()
        throws Exception {
        mockMvc.perform(get(SHORTEN_MAPPING + URL_PARAMETER))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
}
