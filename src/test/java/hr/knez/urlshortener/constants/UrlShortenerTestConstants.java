package hr.knez.urlshortener.constants;

import hr.knez.urlshortener.model.UrlModel;
import hr.knez.urlshortener.resource.UrlModelResource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.hateoas.ResourceSupport;

public class UrlShortenerTestConstants {

    public static final String SHORTEN_MAPPING = "/s/";
    public static final String REDIRECT_MAPPING = "/r/";
    public static final String URL_PARAMETER = "?url=";
    public static final String INVALID_URL = "www.google.com";
    public static final String GOOGLE_URL = "https://google.com";
    public static final LocalDateTime DATE_CREATED =
        LocalDateTime.parse("2019-05-01T10:59:47.981", DateTimeFormatter.ISO_DATE_TIME);
    public static final String INVALID_SHORTURL = "xyz";

    public static final String JSONPATH_LONG_URL_KEY = "$.longUrl";
    public static final String JSONPATH_SHORT_URL_KEY = "$.shortUrl";
    public static final String JSONPATH_CREATED_KEY = "$.created";
    public static final String JSONPATH_CLICKS_KEY = "$.clicks";
    public static final String JSON_GOOGLE_SHORT_URL_VALUE = "n";
    public static final long JSON_CLICK_COUNT_VALUE = 0L;
    public static final long SHORT_URL_ID = 1L;

    public static final ResourceSupport GOOGLE_SHORT_URL_RESOURCE =
        new UrlModelResource(JSON_GOOGLE_SHORT_URL_VALUE, GOOGLE_URL, DATE_CREATED, JSON_CLICK_COUNT_VALUE);

    public static final UrlModel URL_MODEL = new UrlModel(GOOGLE_URL, DATE_CREATED, JSON_CLICK_COUNT_VALUE);
}
